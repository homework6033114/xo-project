import { Routes, Route } from 'react-router-dom'
import Login from './pages/Login'
import Players from './pages/Players'
import Rating from './pages/Rating'
import GamePage from './pages/GamePage'
import History from './pages/History'
import Users from './pages/Users'

function App() {
  return (
    <>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/users" element={<Players />} />
        <Route path="/users/rating" element={<Rating />} />
        <Route path="/game" element={<GamePage />} />
        <Route path="/games/history" element={<History />} />
        <Route path="/admin/users" element={<Users />} />
      </Routes>
    </>
  )
}

export default App
