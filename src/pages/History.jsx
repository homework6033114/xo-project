import React from 'react'
import Header from '../components/Header/Header'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import WinnerIcon from '../components/UI/Icons/WinnerIcon'
import SymbolSmall from '../components/UI/Symbol/SymbolSmall'
import games from '../dev-data/games'

const Rating = () => {
  return (
    <>
      <Header />
      <TableContainer className="history">
        <Title>История игр</Title>
        <div className="table-history">
          <div className="row">
            <div className="players table-headers">Игроки</div>
            <div className="date table-headers">Дата</div>
            <div className="time table-headers">Время игры</div>
          </div>

          {games.map((game) => (
            <div className="row" key={game.id}>
              <div className="players">
                <div className="player">
                  <div className="symbol">
                    <SymbolSmall symbol={game.player1.mode} />
                  </div>
                  <div>{game.player1.userName}</div>

                  {game.player1.isWinner && <WinnerIcon />}
                </div>
                <div className="text-bold">против</div>
                <div className="player">
                  <div className="symbol">
                    <SymbolSmall symbol={game.player2.mode} />
                  </div>
                  <div>{game.player2.userName}</div>
                  {game.player2.isWinner && <WinnerIcon />}
                </div>
              </div>
              <div className="date">{game.date}</div>
              <div className="time">{game.gameTime}</div>
            </div>
          ))}
        </div>
      </TableContainer>
    </>
  )
}

export default Rating
