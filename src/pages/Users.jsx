import React, { useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import Header from '../components/Header/Header'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import Button from '../components/UI/Button/Button'
import GenderIcon from '../components/UI/Icons/GenderIcon'
import StatusM from '../components/UI/Status/StatusM'
import BlockIcon from '../components/UI/Icons/BlockIcon'
import AddUserModal from '../components/Modals/AddUser'
import usersData from '../dev-data/users'

const Users = () => {
  const [users, setUsers] = useState(usersData)
  const [modalIsOpen, setModalIsOpen] = useState(false)

  const onOpenModal = () => {
    setModalIsOpen(true)
  }
  const onCloseModal = () => {
    setModalIsOpen(false)
  }

  const formatDate = (date) => {
    const months = {
      1: 'января',
      2: 'февраля',
      3: 'марта',
      4: 'апреля',
      5: 'мая',
      6: 'июня',
      7: 'июля',
      8: 'августа',
      9: 'сентября',
      10: 'октября',
      11: 'ноября',
      12: 'декабря',
    }
    return (date =
      date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear())
  }

  const addUser = (data) => {
    const newUser = {
      id: uuidv4(),
      ...data,
      userStatus: 'Свободен',
      createdAt: formatDate(new Date()),
      updatedAt: formatDate(new Date()),
    }
    setUsers((prevState) => [...prevState, newUser])
  }

  const updateStatus = (id, status) => {
    let newStatus
    if (status === 'Свободен') {
      newStatus = 'Заблокирован'
    } else if (status === 'Заблокирован') {
      newStatus = 'Свободен'
    }

    setUsers(
      users.map((user) =>
        user.id === id ? { ...user, userStatus: newStatus } : user
      )
    )
  }

  return (
    <>
      <Header />
      <TableContainer className="users-table">
        <div className="header">
          <Title>Список игроков</Title>
          <div className="btn">
            <Button version="primary" onClick={onOpenModal}>
              Добавить игрока
            </Button>
          </div>
        </div>

        <div className="table">
          <div className="row ">
            <div className="user table-headers">ФИО</div>
            <div className="age table-headers">Возраст</div>
            <div className="sex table-headers">Пол</div>
            <div className="status table-headers">Статус</div>
            <div className="date table-headers">Создан</div>
            <div className="date table-headers">Изменен</div>
            <div className="btn"></div>
          </div>
          {users.map((user) => (
            <div className="row " key={user.id}>
              <div className="user">{user.userName}</div>
              <div className="age">{user.age}</div>
              <div className="sex">
                <GenderIcon sex={user.sex} />
              </div>
              <div className="status">
                {user.userStatus === 'Свободен' && (
                  <StatusM status="available">Активен</StatusM>
                )}
                {user.userStatus === 'Заблокирован' && (
                  <StatusM status="blocked">Заблокирован</StatusM>
                )}
              </div>
              <div className="date">{user.createdAt}</div>
              <div className="date">{user.updatedAt}</div>
              <div className="btn">
                {user.userStatus === 'Свободен' ? (
                  <Button
                    version="secondary"
                    onClick={() => updateStatus(user.id, user.userStatus)}
                  >
                    Заблокировать
                  </Button>
                ) : (
                  <Button
                    version="secondary"
                    onClick={() => updateStatus(user.id, user.userStatus)}
                  >
                    <BlockIcon />
                    Разблокировать
                  </Button>
                )}
              </div>
            </div>
          ))}

          {/* {usersData.map((user) => (
            <div className="row " key={user.id}>
              <div className="user">{user.userName}</div>
              <div className="age">{user.age}</div>
              <div className="sex">
                <GenderIcon sex={user.sex} />
              </div>
              <div className="status">
                {user.userStatus === 'Свободен' && (
                  <StatusM status="available">Активен</StatusM>
                )}
                {user.userStatus === 'Заблокирован' && (
                  <StatusM status="blocked">Заблокирован</StatusM>
                )}
              </div>
              <div className="date">{user.createdAt}</div>
              <div className="date">{user.updatedAt}</div>
              <div className="btn">
                {user.userStatus === 'Свободен' ? (
                  <Button
                    version="secondary"
                    onClick={() => updateStatus(user.id)}
                  >
                    Заблокировать
                  </Button>
                ) : (
                  <Button
                    version="secondary"
                    onClick={() => updateStatus(user.id)}
                  >
                    <BlockIcon />
                    Разблокировать
                  </Button>
                )}
              </div>
            </div>
          ))} */}

          {/* {usersData.map((user) => (
            <div className="row " key={user.id}>
              <div className="user">{user.userName}</div>
              <div className="age">{user.age}</div>
              <div className="sex">
                <GenderIcon sex={user.sex} />
              </div>
              <div className="status">
                {user.userStatus === 'Свободен' ? (
                  <StatusM status="available">Активен</StatusM>
                ) : (
                  <StatusM status="blocked">{user.userStatus}</StatusM>
                )}
              </div>
              <div className="date">{user.createdAt}</div>
              <div className="date">{user.updatedAt}</div>
              <div className="btn">
                {user.userStatus === 'Свободен' ? (
                  <Button version="secondary" onClick={toggleStatus}>
                    Заблокировать{' '}
                  </Button>
                ) : (
                  <Button version="secondary" onClick={toggleStatus}>
                    <BlockIcon />
                    Разблокировать
                  </Button>
                )}
              </div>
            </div>
          ))} */}
        </div>
      </TableContainer>
      <AddUserModal
        onClose={onCloseModal}
        modalIsOpen={modalIsOpen}
        addUser={addUser}
        setModalIsOpen={setModalIsOpen}
      />
    </>
  )
}

export default Users
