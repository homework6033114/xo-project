import React from 'react'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import Header from '../components/Header/Header'
import users from '../dev-data/users'

const Rating = () => {
  return (
    <>
      <Header />
      <TableContainer className="rating">
        <Title>Рейтинг игроков</Title>

        <div className="table-rating">
          <div className="row">
            <div className="userName table-headers">ФИО</div>
            <div className="totalGames table-headers">Всего игр</div>
            <div className="wins table-headers">Победы</div>
            <div className="losses table-headers">Проигрыши</div>
            <div className="winRate table-headers">Процент побед</div>
          </div>

          {users.map((user) => (
            <div className="row" key={user.id}>
              <div className="userName">{user.userName}</div>
              <div className="totalGames">{user.totalGames}</div>
              <div className="wins success">{user.wins}</div>
              <div className="losses danger">{user.losses}</div>
              <div className="winRate">{user.winRate}%</div>
            </div>
          ))}
        </div>
      </TableContainer>
    </>
  )
}

export default Rating
