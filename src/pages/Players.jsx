import React, { useState } from 'react'
import Header from '../components/Header/Header'
import TableContainer from '../components/UI/Table/TableContainer'
import Title from '../components/UI/Text/Title'
import StatusM from '../components/UI/Status/StatusM'
import Button from '../components/UI/Button/Button'
import SwitchToggle from '../components/UI/SwitchToggle/SwitchToggle'
import usersData from '../dev-data/users'

const Players = () => {
  const [isChecked, setIsChecked] = useState(false)

  const players = usersData.filter(
    (player) => player.playerStatus !== 'Вне игры'
  )
  const availablePlayers = players.filter(
    (player) => player.playerStatus === 'Готов играть'
  )
  const onChange = (e) => {
    setIsChecked(!isChecked)
  }

  return (
    <>
      <Header />
      <TableContainer className="activePlayers">
        <div className="header">
          <Title>Активные игроки</Title>
          <div className="switchToggle">
            <p>Только свободные</p>
            <SwitchToggle onChange={onChange} isChecked={isChecked} />
          </div>
        </div>
        <div className="table-activePlayers">
          {!isChecked
            ? players.map((player) => (
                <div className="row" key={player.id}>
                  <div className="player">{player.userName}</div>
                  <div className="status">
                    {player.playerStatus === 'Готов играть' && (
                      <StatusM status="available">Свободен</StatusM>
                    )}
                    {player.playerStatus === 'Играет' && (
                      <StatusM status="playing">В игре</StatusM>
                    )}
                  </div>
                  <div className="btn">
                    {player.playerStatus === 'Готов играть' && (
                      <Button version="primary">Позвать играть</Button>
                    )}
                    {player.playerStatus === 'Играет' && (
                      <Button version="secondary" isDisabled={true}>
                        Позвать играть
                      </Button>
                    )}
                  </div>
                </div>
              ))
            : availablePlayers.map((player, index) => (
                <div className="row" key={index}>
                  <div className="player">{player.userName}</div>
                  <div className="status">
                    <StatusM status="available">Свободен</StatusM>
                  </div>
                  <div className="btn">
                    <Button version="primary">Позвать играть</Button>
                  </div>
                </div>
              ))}
        </div>
      </TableContainer>
    </>
  )
}

export default Players
