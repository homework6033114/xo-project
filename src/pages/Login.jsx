import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Input from '../components/UI/Input/Input'
import Button from '../components/UI/Button/Button'
import Title from '../components/UI/Text/Title'

const Login = () => {
  const navigate = useNavigate()
  const [formdata, setFormData] = useState({
    login: '',
    password: '',
  })
  const { login, password } = formdata

  const [formInputsIsValid, setFormInputsIsValid] = useState({
    login: true,
    password: true,
  })

  // TODO: onKeyPress deprecated - change validation
  // Запрет на ввод символов, кроме английских букв, цифр и знаков: точка, нижнее подчеркивание:
  // const keyPressHandler = (e) => {
  //   const key = e.keyCode
  //   if (
  //     key !== 46 &&
  //     key !== 95 &&
  //     (key < 48 || key > 57) &&
  //     (key < 65 || key > 90) &&
  //     (key < 97 || key > 122)
  //   ) {
  //     alert(
  //       'Допустимо вводить только английские буквы, цифры и знаки: точка, нижнее подчеркивание'
  //     )
  //     e.preventDefault()
  //   }
  // }

  // Запрет на вставку скопированных значений, чтобы предовратить ввод запрещенных символов:
  const pasteHandler = (e) => {
    alert('Нельзя вставлять скопированные значения в поле')
    e.preventDefault()
  }

  // Запрет на копирование введенных значений:
  const copyHandler = (e) => {
    alert('Нельзя копировать введенные значения  из поля.')
    e.preventDefault()
  }

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()

    // TODO: check password match

    const isEmpty = (value) => value.trim() === ''
    const loginIsValid = !isEmpty(login)
    const passwordIsValid = !isEmpty(password)

    setFormInputsIsValid({
      login: loginIsValid,
      password: passwordIsValid,
    })

    if (loginIsValid && passwordIsValid) {
      const userData = {
        login,
        password,
      }
      console.log(userData)

      setFormData({
        login: '',
        password: '',
      })
      navigate('/users')
    }
  }
  return (
    <div className="center">
      <div className="login-container">
        <div className="img">
          <img src="../imgs/dog.svg" alt="dog" />
        </div>
        <Title>Войдите в игру</Title>
        <form className="login-form" onSubmit={onSubmit}>
          <div className="inputs-group">
            <div className="input-control">
              <Input
                type="text"
                name="login"
                value={login}
                isValid={formInputsIsValid.login}
                placeholder="Логин"
                message="Неверный логин"
                // onKeyPress={keyPressHandler}
                onPaste={pasteHandler}
                onCopy={copyHandler}
                onChange={onChange}
              />
              {!formInputsIsValid.login && (
                <p className="error-message">Неверный логин</p>
              )}
            </div>
            <div className="input-control">
              <Input
                isValid={formInputsIsValid.password}
                value={password}
                name="password"
                type="password"
                placeholder="Пароль"
                message="Неверный пароль"
                // onKeyPress={keyPressHandler}
                onPaste={pasteHandler}
                onCopy={copyHandler}
                onChange={onChange}
              />
              {!formInputsIsValid.password && (
                <p className="error-message">Неверный пароль</p>
              )}
            </div>
          </div>
          <Button
            type="submit"
            isDisabled={!login || !password}
            // isDisabled={false}
            version="primary"
          >
            Войти
          </Button>
        </form>
      </div>
    </div>
  )
}

export default Login
