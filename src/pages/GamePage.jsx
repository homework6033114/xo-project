import React, { useEffect, useState } from 'react'
import Header from '../components/Header/Header'
import PlayersList from '../components/PlayersList/PlayersList'
import GameBoard from '../components/Game/GameBoard'
import Chat from '../components/Chat/Chat'
import GameContext from '../context/GameContext'
import game from '../modules/Game'
import mockedGameApi from '../MockAPI/mockGame'
import Spinner from '../components/UI/Spinner/Spinner'

const Game = () => {
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    mockedGameApi.getPlayersInfo().then((data) => {
      game.playersInfo = data
      setIsLoading(false)
    })
  }, [])

  if (isLoading) {
    return <Spinner />
  }

  return (
    <>
      <Header />
      <GameContext.Provider value={game}>
        <main>
          <div className="main-container">
            <PlayersList />
            <GameBoard />
            <Chat />
          </div>
        </main>
      </GameContext.Provider>
    </>
  )
}

export default Game
