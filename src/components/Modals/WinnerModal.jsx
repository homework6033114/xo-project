import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import './Modal.css'
import Title from '../UI/Text/Title'
import Button from '../UI/Button/Button'
import GameContext from '../../context/GameContext'

const WinnerModal = ({ modalIsOpen, playAgain, player1, player2 }) => {
  const game = useContext(GameContext)
  let winner = game.winner === 'X' ? player1 : player2

  const navigate = useNavigate()

  const goToMenu = () => {
    game.resetGame()
    navigate('/users')
  }

  return (
    <div className={`backdrop ${modalIsOpen ? 'close' : 'open'}`}>
      <div className="modalContainer">
        <div className="modal modal-winner">
          {game.winner && (
            <>
              <div>
                <img src="../imgs/trophy.svg" alt="Победитель" />
              </div>
              <Title>{winner} победил! </Title>
            </>
          )}

          {game.isTie && <Title>Ничья </Title>}
          <Button onClick={playAgain} version="primary">
            Новая игра
          </Button>
          <Button onClick={goToMenu} version="secondary">
            Выйти в меню
          </Button>
        </div>
      </div>
    </div>
  )
}

export default WinnerModal
