import React from 'react'
import './Icon.css'
const WinnerIcon = () => {
  return (
    <div>
      <img className="icon" src="../imgs/winner.svg" alt="winner" />
    </div>
  )
}

export default WinnerIcon
