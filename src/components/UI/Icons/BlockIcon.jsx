import React from 'react'
import './Icon.css'

const BlockIcon = () => {
  return (
    <div>
      <div>
        <img className="icon" src="../imgs/block.svg" alt="blocked" />
      </div>
    </div>
  )
}

export default BlockIcon
