import React from 'react'
import './Icon.css'
const GenderIcon = ({ sex }) => {
  return (
    <div>
      <img
        className="icon"
        src={`${sex === 'M' ? '../imgs/male.svg' : '../imgs/female.svg'}`}
        alt="gender icon"
      />
    </div>
  )
}

export default GenderIcon
