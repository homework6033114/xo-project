import React from 'react'
import './Input.css'

const Input = ({
  type,
  name,
  id,
  placeholder,
  isValid,
  value,
  onKeyPress,
  onPaste,
  onCopy,
  onInput,
  onKeyDown,
  onChange,
}) => {
  return (
    <input
      className={`input ${!isValid ? 'error' : ''}`}
      type={type}
      name={name}
      id={id}
      value={value}
      placeholder={placeholder}
      onKeyPress={onKeyPress}
      onKeyDown={onKeyDown}
      onPaste={onPaste}
      onCopy={onCopy}
      onInput={onInput}
      onChange={onChange}
    />
  )
}

export default Input
