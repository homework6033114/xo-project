import React from 'react'
import './Text.css'

const Title = ({ children }) => {
  return <h1>{children}</h1>
}

export default Title
