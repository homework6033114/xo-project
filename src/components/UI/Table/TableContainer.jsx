import React from 'react'
import './TableContainer.css'

const TableContainer = ({ className, children }) => {
  return <div className={`table-container ${className}`}>{children}</div>
}

export default TableContainer
