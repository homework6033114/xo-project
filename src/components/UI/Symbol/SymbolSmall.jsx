import React from 'react'

const SymbolSmall = ({ symbol }) => {
  if (symbol === 'X') {
    return <img src="../imgs/x.svg" alt="X symbol" />
  } else if (symbol === 'O') {
    return <img src="../imgs/zero.svg" alt="zero symbol" />
  } else {
    return null
  }
}

export default SymbolSmall
