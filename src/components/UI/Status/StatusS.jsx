import React from 'react'
import './Status.css'

const StatusS = ({ status }) => {
  return <div className={`status status-S ${status}`} />
}

export default StatusS
