import React from 'react'
import './Button.css'

const Button = ({ children, type, isDisabled, size, version, onClick }) => {
  return (
    <button
      className={`btn-${version} btn-${size}`}
      type={type}
      disabled={isDisabled}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

export default Button
