import React from 'react'
import './MessageContainer.css'

const MessageContainer = ({ message }) => {
  const { time, text, player } = message

  const getShortName = (name) => {
    return `${name.split(' ')[0]} ${name.split(' ')[1]}`
  }
  const userShortName = getShortName(player.userName)

  return (
    <div className={`msg-container ${player.isCurrentUser ? 'me' : 'other'}`}>
      <div className="msg-header">
        <div className={`subject-name ${player.mode === 'X' ? 'x' : 'zero'}`}>
          {userShortName}
        </div>
        <div className="time">{time}</div>
      </div>
      <div className="msg-body">{text}</div>
    </div>
  )
}

export default MessageContainer
