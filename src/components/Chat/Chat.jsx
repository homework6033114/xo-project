import React, { useContext, useState } from 'react'
import { observer } from 'mobx-react-lite'
import { v4 as uuidv4 } from 'uuid'
import MessageContainer from './MessageContainer/MessageContainer'
import Input from '../UI/Input/Input'
import Button from '../UI/Button/Button'
import './Chat.css'
import GameContext from '../../context/GameContext'
import mockedGameApi from '../../MockAPI/mockGame'

const Chat = () => {
  const game = useContext(GameContext)

  const [player1, player2] = game.players

  const [text, setText] = useState('')

  const onChange = (e) => {
    setText(e.target.value)
  }

  let time =
    new Date().getHours() +
    ':' +
    String(new Date().getMinutes()).padStart(2, '0')

  const sendMessage = (e) => {
    e.preventDefault()

    const player = game.mode === 'X' ? player1 : player2

    const newMessage = {
      id: uuidv4(),
      player: player,
      time,
      text,
    }
    mockedGameApi.sendMessage(newMessage)

    setText('')
  }

  return (
    <div className="chat-container">
      <div className="msgs-container">
        {game.messages.length === 0 && (
          <div className="text">Сообщений еще нет</div>
        )}
        {game.messages.map((message) => (
          <MessageContainer key={message.id} message={message} />
        ))}
      </div>

      <div className="msg-interactive-elements">
        <Input
          type="text"
          value={text}
          placeholder="Сообщение..."
          isValid={true}
          onChange={onChange}
        />
        <Button version="primary" onClick={sendMessage}>
          <img src="../imgs/send-btn.svg" alt="кнопка отправить" />
        </Button>
      </div>
    </div>
  )
}

export default observer(Chat)
