import React, { useContext } from 'react'
import Title from '../UI/Text/Title'
import SymbolSmall from '../UI/Symbol/SymbolSmall'
import PlayerInfo from './PlayerInfo'
import './PlayersList.css'
import GameContext from '../../context/GameContext'

const PlayersList = () => {
  const game = useContext(GameContext)
  const [player1, player2] = game.players

  return (
    <div className="subject-list">
      <Title>Игроки</Title>
      <div className="container">
        <div className="subject-container">
          <div className="symbol">
            <SymbolSmall symbol="X" />
          </div>
          <PlayerInfo name={player1.userName} winRate={player1.winRate} />
        </div>
        <div className="subject-container">
          <div className="symbol">
            <SymbolSmall symbol="O" />
          </div>
          <PlayerInfo name={player2.userName} winRate={player2.winRate} />
        </div>
      </div>
    </div>
  )
}

export default PlayersList
