import React, { useState, useContext } from 'react'
import { reaction } from 'mobx'
import { observer } from 'mobx-react-lite'

import Field from './Field'
import SymbolSmall from '../UI/Symbol/SymbolSmall'
import WinnerModal from '../Modals/WinnerModal'
import Timer from './Timer'
import './Game.css'
import GameContext from '../../context/GameContext'
import mockGameApi from '../../MockAPI/mockGame'

const getShortName = (name) => {
  return `${name.split(' ')[0]} ${name.split(' ')[1]}`
}

const GameBoard = () => {
  const game = useContext(GameContext)
  const [player1, player2] = game.players

  const player1Name = getShortName(player1.userName)
  const player2Name = getShortName(player2.userName)

  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [time, setTime] = useState(0)

  // TODO: change Timer
  // const [isRunning, setIsRunning] = useState(true)

  // useEffect(() => {
  //   let intervalId
  //   if (isRunning) {
  //     intervalId = setInterval(() => setTime(time + 1), 10)
  //   }
  //   return () => clearInterval(intervalId)
  // }, [isRunning, time])

  // const resetTimer = () => {
  //   setTime(0)
  //   setIsRunning(true)
  // }

  const play = (i) => {
    if (game.field[i] !== null) {
      mockGameApi.moveFailed()
      return
    }
    mockGameApi.makeMove(player1.id, i)
    mockGameApi.moveMade(player1.id, i)

    reaction(
      () => game.isOver,
      (isOver) => {
        if (isOver) {
          setModalIsOpen(true)
          // setIsRunning(false)
        } else {
          setModalIsOpen(false)
          // setIsRunning(true)
        }
      }
    )
  }

  const playAgain = () => {
    game.resetGame()
  }

  return (
    <>
      <div className="game-container">
        <Timer time={time} />
        <Field play={play} />
        <div className="game-step">
          Ходит&nbsp;
          <SymbolSmall symbol={game.mode} />
          &nbsp;
          {game.mode === 'X' ? player1Name : player2Name}
        </div>
      </div>
      {game.isOver && (
        <WinnerModal
          modalIsOpen={modalIsOpen}
          playAgain={playAgain}
          player1={player1Name}
          player2={player2Name}
        />
      )}
    </>
  )
}

export default observer(GameBoard)
