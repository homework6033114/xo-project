import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import Cell from './Cell/Cell'
import GameContext from '../../context/GameContext'

const Field = ({ play }) => {
  const game = useContext(GameContext)

  return (
    <div className="game-board">
      {game.field.map((_, i) => (
        <Cell key={i} i={i} onClick={(e) => play(i)} />
      ))}
    </div>
  )
}

export default observer(Field)
