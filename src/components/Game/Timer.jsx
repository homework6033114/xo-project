import React from 'react'

const Timer = ({ time }) => {
  const minutes = Math.floor((time % 360000) / 6000)
    .toString()
    .padStart(2, '0')

  const seconds = Math.floor((time % 6000) / 100)
    .toString()
    .padStart(2, '0')

  return (
    <div className="game-time">
      {minutes}:{seconds}
    </div>
  )
}

export default Timer
