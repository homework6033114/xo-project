import React from 'react'
import Tab from '../UI/Tab/Tab'
import Button from '../UI/Button/Button'
import './Header.css'

const Header = () => {
  return (
    <header>
      <div className="logo">
        <img src="../imgs/s-logo.svg" alt="logo" />
      </div>
      <div className="nav-panel">
        <Tab path="/game">Игровое поле</Tab>
        <Tab path="/users/rating">Рейтинг</Tab>
        <Tab path="/users">Активные игроки</Tab>
        <Tab path="/games/history">История игр</Tab>
        <Tab path="/admin/users">Список игроков</Tab>
      </div>
      <Button version="icon">
        <img src="../imgs/signout-icon.svg" alt="sign-up button" />
      </Button>
    </header>
  )
}

export default Header
