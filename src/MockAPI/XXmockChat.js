class Chat {
  messages = []

  emit(type, payload) {
    this.messages[type].forEach((cb) => cb(type, payload, this))
  }
  on(type, cb) {
    this.messages[type].push(cb)
  }
}

class MockChatApi {
  chat
  _job

  constructor() {
    this.chat = new Chat()

    this.chat.on('send-message', ({ text }) => {
      setTimeout(() => {
        this.chat.emit('message-sent', { message: 'Привет' })
      }, 1000)
    })
    this._rerunJob()
  }

  sendMessage(message) {
    this.chat.emit('send-message', { message })
    this._rerunJob()
  }
  onMessage(cb) {
    this.chat.on('message-sent', cb)
  }
  _rerunJob() {
    if (this._job) {
      clearTimeout(this._job)
    }

    this._job = setTimeout(() => {
      this.chat.emit('message-sent', { message: 'Ты тут?' })
      this._rerunJob()
    }, 10000)
  }
}

class WsChatApi {
  constructor(socket) {
    this.socket = socket
  }

  sendMessage(message) {
    this.socket.emit('send-message', { message })
  }

  onMessage(cb) {
    this.socket.on('message-sent', cb)
  }
}

const mockChatApi = new MockChatApi()

export default mockChatApi
