import { makeAutoObservable, observable, action, computed } from 'mobx'
import { makePersistable, stopPersisting } from 'mobx-persist-store'

class Game {
  mode = 'X'
  field = [null, null, null, null, null, null, null, null, null]
  players
  messages = []

  constructor() {
    makeAutoObservable(this, {
      mode: observable,
      field: observable,
      players: observable,
      messages: observable,

      makeMove: action,
      winner: computed,
      isTie: computed,
      isOver: computed,
      resetGame: action,
    })
    makePersistable(this, {
      name: 'GameStore',
      properties: ['mode', 'field', 'players', 'messages'],
      storage: window.localStorage,
      expireIn: 86400000,
      removeOnExpiration: true,
    })
  }

  makeMove(i) {
    this.field[i] = this.mode
    this.mode = this.mode === 'X' ? 'O' : 'X'
  }

  get winner() {
    if (this.isWinner('X')) {
      return 'X'
    }
    if (this.isWinner('O')) {
      return 'O'
    }
    return null
  }

  get isTie() {
    return !this.winner && !this.field.includes(null)
  }

  get isOver() {
    return this.winner || !this.field.includes(null)
  }

  isWinner(symbol) {
    const combs = [
      // Rows
      ['0', '1', '2'],
      ['3', '4', '5'],
      ['6', '7', '8'],
      // Columns
      ['0', '3', '6'],
      ['1', '4', '7'],
      ['2', '5', '8'],
      // Diagonal
      ['0', '4', '8'],
      ['2', '4', '6'],
    ]

    for (let comb of combs) {
      if (
        this.field[comb[0]] !== null &&
        this.field[comb[0]] === this.field[comb[1]] &&
        this.field[comb[1]] === this.field[comb[2]]
      ) {
        return this.field[comb[0]] === symbol
      }
    }
  }

  set playersInfo(value) {
    this.players = value
  }
  sendMessage(newMessage) {
    this.messages = [...this.messages, newMessage]
  }

  resetGame() {
    this.mode = 'X'
    this.field = [null, null, null, null, null, null, null, null, null]
    this.messages = []
  }

  // stopStore() {
  //   stopPersisting(this)
  // }
}

const game = new Game()

export default game
