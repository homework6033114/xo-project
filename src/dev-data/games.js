const games = [
  {
    id: 1,
    player1: {
      userName: 'Терещенко У.Р.',
      mode: 'O',
      isWinner: true,
    },
    player2: {
      userName: 'Многогрешный П. В.',
      mode: 'X',
    },
    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
    isWinner: false,
  },

  {
    id: 2,
    player1: {
      userName: 'Горбачёв А. Д',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Многогрешный П. В.',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },

  {
    id: 3,
    player1: {
      userName: 'Константинов В. Н.',
      mode: 'O',
      isWinner: true,
    },
    player2: {
      userName: 'Сасько Ц. А.',
      mode: 'X',
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 4,
    player1: {
      userName: 'Никифорова Б. А.',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Горбачёв А. Д',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 5,
    player1: {
      userName: 'Кулишенко К. И.',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Вишняков Ч. М.',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 6,
    player1: {
      userName: 'Гриневска Д. Б.',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Кудрявцев Э. В.',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 7,
    player1: {
      userName: 'Нестеров Х. А.',
      mode: 'O',
      isWinner: true,
    },
    player2: {
      userName: 'Исаева О. А.',
      mode: 'X',
      isWinner: false,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 8,

    player1: {
      userName: 'Исаева О. А.',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Кулишенко К. И.',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 9,
    player1: {
      userName: 'Коновалова В. В.',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Терещенко У. Р.',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 10,
    player1: {
      userName: 'Казаков Х. Е.',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Овчаренко Б. М.',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 11,
    player1: {
      userName: 'Казаков Х. Е.',
      mode: 'O',
      isWinner: false,
    },
    player2: {
      userName: 'Никифорова Б. А.',
      mode: 'X',
      isWinner: true,
    },

    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
]

export default games
