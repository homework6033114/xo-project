export const mockPlayers = [
  {
    id: 1,
    userName: 'Плюшкина Екатерина Викторовна',
    mode: 'X',
    winRate: '23',
    isCurrentUser: true,
  },

  {
    id: 2,
    userName: 'Пупкин Владлен Игоревич',
    mode: 'O',
    winRate: '63%',
    isCurrentUser: false,
  },
]
